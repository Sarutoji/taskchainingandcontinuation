﻿using System;
using System.Threading.Tasks;

namespace TaskChaining
{
#pragma warning disable S1118 // Utility classes should not have public constructors
    public class Program
#pragma warning restore S1118 // Utility classes should not have public constructors
    {
        static Random random = new Random();

        static void Main(string[] args)
        {
            Task<int[]> task1 = Task.Run(() =>
            {
                return CreateArray();
            });
            Task<int[]> task2 = task1.ContinueWith(x =>
            {
                return MultiplyArray(task1.Result);
            });
            Task<int[]> task3 = task2.ContinueWith(x =>
            {
                return SortArrayAscending(task2.Result);
            });
            Task<double> task4 = task3.ContinueWith(x =>
            {
                return CalculateAvgOfArray(task3.Result);
            });
            task1.Wait();

            Console.WriteLine("Result of all four tasks: {0}", task4.Result);
        }

        public static double CalculateAvgOfArray(int[] array)
        {
            if (array == null || Array.Empty<int>() == array)
            {
                throw new ArgumentNullException("array");
            }
            if (array.Length > 10 || array.Length < 10)
            {
                throw new ArgumentException("Array lenght must be 10");
            }
            for (int j = 0; j < array.Length; j++)
            {
                if (array[j] <= 0)
                {
                    throw new ArgumentException("Values in array must be higher than 0", nameof(array));
                }
            }

            double sum = 0;
            for (int i = 0; i < array.Length; i++)
            {
                sum += array[i];
            }

            return sum / 10;
        }

        public static int[] SortArrayAscending(int[] array)
        {
            if (array == null || Array.Empty<int>() == array)
            {
                throw new ArgumentNullException("array");
            }
            if (array.Length > 10 || array.Length < 10)
            {
                throw new ArgumentException("Array lenght must be 10");
            }
            Array.Sort(array);

            return array;
        }

        public static int[] MultiplyArray(int[] array)
        {
            if (array == null || Array.Empty<int>() == array)
            {
                throw new ArgumentNullException("array");
            }
            if (array.Length > 10 || array.Length < 10)
            {
                throw new ArgumentException("Array lenght must be 10");
            }
            for (int j = 0; j < array.Length; j++)
            {
                if (array[j] <= 0)
                {
                    throw new ArgumentException("Values in array must be higher than 0", nameof(array));
                }
            }
            int multiplier = random.Next(1, 20);
            for (int i = 0; i < array.Length; i++)
            {
                array[i] *= multiplier;
            }

            return array;
        }

        private static int[] CreateArray()
        {
            int[] array = new int[10];
            for (int i = 0; i < 10; i++)
            {
                array[i] = random.Next(10, 100);
            }

            return array;
        }
    }
}
